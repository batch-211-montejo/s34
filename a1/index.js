const express = require("express")

const app = express()

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.get('/home', (request, response) => {
	response.send("Welcome to the home page")
})


let users = 
    {
        "username" : 'johndoe', 
        "password" : 'johndoe123'
    }



app.get('/users', (request, response) => {
	response.send(users)
})


app.delete('/delete-user', (request, response) => {
	response.send(`User ${users.username} has been deleted`)
})


app.listen(port, () => console.log(`Server running at port ${port}`))
